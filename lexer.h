/*
** lexer.h for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 sanche_g
** Last update 2012-10-08 04:52 vermeille
*/

#ifndef LEXER_H_
# define LEXER_H_

# include "stream.h"
# include "ast.h"

typedef enum
{
    T_ID,
    T_NUMBER,
    T_OP,
    T_CHAR,
    T_TYPE,
} e_token_type;

typedef struct
{
    e_token_type type;
    union
    {
        char   *as_id;
        int    as_nbr;
        e_op   as_op;
        char   as_char;
        e_type as_type;
    };
} s_token;

typedef struct
{
    t_stream     stream;
    int          index;
    s_token      token;
} s_tokenizer;

typedef s_tokenizer *t_tokenizer;

void eat_whitespaces(t_stream s);
void lex_op(t_tokenizer s);
void lex(t_tokenizer t);
s_token current_token(t_tokenizer t);

#endif /* !LEXER_H_ */

