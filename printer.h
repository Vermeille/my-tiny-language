/*
** printer.h for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 sanche_g
** Last update 2012-10-07 19:05 vermeille
*/

#ifndef PRINTER_H_
# define PRINTER_H_

# include "ast.h"

void print(s_ast *ast);
void print_binop(e_op bop);
void print_value(s_value *ast);

#endif /* !PRINTER_H_ */

