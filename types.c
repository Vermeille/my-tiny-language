/*
** types.c for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 19:31 sanche_g
** Last update 2012-10-07 21:36 vermeille
*/

#include "types.h"

int is_whitespace(char c)
{
    return (c == ' ' || c == '\t' || c == '\n');
}

int is_number(char c)
{
    return ('0' <= c && c <= '9');
}

int is_alpha(char c)
{
    return (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '_');
}

int is_binop(e_op c)
{
    return (c == ADD || c == SUB || c == MUL || c == DIV || c == MOD);
}

int is_unop(e_op c)
{
    return (c == ADD || c == SUB);
}

int is_high_op(e_op c)
{
    return (c == MUL || c == DIV || c == MOD);
}

int is_low_op(e_op c)
{
    return (c == ADD || c == SUB);
}

int is_comp_op(e_op c)
{
    return (c == LESS || c == MORE || c == EQ);
}

int is_sym(char c)
{
    return (c == '<' || c == '>' || c == '=' || c == '+' ||
            c == '-' || c == '*' || c == '/' || c == '%' || c == '=' ||
            c == '(' || c == ')');
}
