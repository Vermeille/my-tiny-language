#
# Makefile
# sanche_g, 2012-10-06 19:25
#
TARGET=lang
CFLAGS=-g -Wall -std=c99 -Wextra
CC=clang
OBJ=main.o lexer.o stream.o types.o printer.o eval.o eval_types.o error.o parser.o

all:$(OBJ)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJ)

clean:
	-rm *.o *.swp

distclean: clean
	-rm lang

%.o:%.c %.h

# vim:ft=make
#

