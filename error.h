/*
** error.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 vermeille
** Last update 2012-10-07 12:48 vermeille
*/

#ifndef ERROR_H_
# define ERROR_H_

void error(int err, char *fmt, va_list ap);

#endif /* !ERROR_H_ */

