/*
** stream.c for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 19:29 sanche_g
** Last update 2012-10-07 18:23 vermeille
*/

#include <stdlib.h>

#include "stream.h"

char current_char(t_stream s)
{
    return (s->content[s->index]);
}

void eat_char(t_stream s)
{
    ++s->index;
}

t_stream stream_init(char *str)
{
    t_stream strm;

    strm = malloc(sizeof (s_stream));
    strm->content = str;
    strm->index = 0;

    return (strm);
}

void stream_destroy(t_stream s)
{
    free(s->content);
    free(s);
}
