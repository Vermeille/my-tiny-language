/*
** list.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-08 vermeille
** Last update 2012-10-08 18:17 vermeille
*/

#include <stdlib.h>

typedef struct
{
    char *name;
} s_name_lookup;

typedef struct
{
    s_name_lookup *names;
    int size;
    int max_size;
} s_scope;

typedef struct
{
    s_scope *scopes;
    int size;
    int max_size;
} *s_name_resolv;

s_name_resolv name_resolv_init(void)
{
    s_name_resolv nr;

    nr = malloc(sizeof (*nr));
    nr->scopes = malloc(sizeof (s_scope) * 4);
    nr->size = 0;
    nr->max_size = 4;

    for (int i = 0; i < 8; ++i)
    {
        nr->scopes[i].names = malloc(sizeof (s_name_lookup) * 4);
        nr->scopes[i].max_size = 4;
        nr->scopes[i].size = 0;
    }

    return (nr);
}

void name_add(s_name_resolv l, char *name)
{
    if (l->size == l->max_size)
    {
        l->max_size *= 2;
        l->scopes = realloc(l->scopes, l->max_size * sizeof (s_scope));
    }

    s_scope *cur = &l->scopes[l->size - 1];

    if (cur->size == cur->max_size)
    {
        cur->max_size *= 2;
        cur->names = realloc(cur->names, l->max_size * sizeof (s_name_lookup));
    }


    cur->names[cur->size].name = name;
    ++cur->size;
}

void nr_push_scope(s_name_resolv nr)
{
    if (nr->size == nr->max_size)
    {
        nr->max_size *= 2;
        nr->scopes = realloc(nr->scopes, nr->max_size * sizeof (s_scope));
    }
    ++nr->size;
}


void list_##Type##_del(s_list_##Type l, int n)
{
    l->values[n] = l->values[size - 1];
    --l->size;
    if (size < max_size / 2)
    {
        max_size /= 2;
        l->values = realloc(l->values, max_size);
    }
}

void list_##Type##_destroy(s_list_##Type l)
{
    free(l->values);
    free(l);
}


#endif /* !LIST_H_ */

