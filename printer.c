/*
** printer.c for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 20:06 sanche_g
** Last update 2012-10-07 21:19 vermeille
*/

#include <stdio.h>

#include "printer.h"

void print_binop(e_op bop)
{
    switch (bop)
    {
        case ADD:
            printf(" + ");
            break;
        case SUB:
            printf(" - ");
            break;
        case MUL:
            printf(" * ");
            break;
        case DIV:
            printf(" / ");
            break;
        case MOD:
            printf(" %% ");
            break;
        case LESS:
            printf(" < ");
            break;
        case MORE:
            printf(" > ");
            break;
        case LEQ:
            printf(" <= ");
            break;
        case MOQ:
            printf(" >= ");
            break;
        case EQ:
            printf(" == ");
            break;
        case NEQ:
            printf(" != ");
            break;
        default:
            printf(" £ ");
    }
}

void print_value(s_value *val)
{
    switch (val->type)
    {
        case NUMBER:
            printf("%d", val->as_int);
            break;
        case CHAR:
            printf("'%c'", val->as_char);
            break;
        case STRING:
            printf("\"%s\"", val->as_str);
            break;
        case BOOL:
            printf(val->as_int ? "true" : "false");
            break;
    }
}

void print(s_ast *ast)
{
    switch (ast->type)
    {
        case VALUE:
            print_value(&ast->as_value);
            break;

        case UNOP:
            printf("-");
            print(ast->as_unop.rhs);
            break;

        case BINOP:
            printf("(");
            print(ast->as_bop.lhs);
            print_binop(ast->as_bop.op);
            print(ast->as_bop.rhs);
            printf(")");
            break;

        case VAR:
            printf("%s", ast->as_id.id);
            break;
        case FUNCALL:
            // FIXME
            break;
    }
}

