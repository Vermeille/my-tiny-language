/*
** main.c for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 19:02 sanche_g
** Last update 2012-10-07 22:05 vermeille
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "ast.h"
#include "printer.h"
#include "lexer.h"
#include "parser.h"
#include "stream.h"
#include "eval.h"

char *get_stdin()
{
    const int buf_size = 512;
    int size = 0;
    char *res = NULL;

    do
    {
        size += buf_size;
        res = realloc(res, size);
    }
    while (read(STDIN_FILENO, res + size - buf_size, buf_size));

    return (res);
}

int main(int argc, char const *argv[])
{
    s_tokenizer tok;
    s_ast *ast;

    tok.index = 0;
    tok.stream = stream_init(get_stdin());
    lex(&tok);
    ast = parse_comp(&tok);

    print(ast);
    printf("\n\n== Evaluation ==\n\n");

    s_value i_miss_rvalues_references = eval(ast);
    print_value(&i_miss_rvalues_references);

    printf("\n");
    stream_destroy(tok.stream);
    return (0);
}

