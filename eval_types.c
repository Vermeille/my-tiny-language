/*
** eval_types.c for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 02:36 vermeille
** Last update 2012-10-07 22:06 vermeille
*/

#include "eval_types.h"

e_value_type eval_type_binop(s_ast *ast)
{
    e_value_type lhs = eval_type(ast->as_bop.lhs);
    e_value_type rhs = eval_type(ast->as_bop.rhs);

    if (lhs == rhs)
        return (lhs);
    // FIXME Error

    return (lhs);
}

e_value_type eval_type(s_ast *ast)
{
    switch (ast->type)
    {
        case NUMBER:
            return (NUMBER);

        case BINOP:
            return (eval_type_binop(ast));

        case UNOP:
            return (eval_type(ast->as_unop.rhs));

        case VAR:
            // FIXME
            break;

        case FUNCALL:
            // FIXME funcall
            break;
    }

    return (666);
}

