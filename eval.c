/*
** eval.c for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 02:04 vermeille
** Last update 2012-10-07 22:08 vermeille
*/

#include <stdlib.h>
#include <stdio.h>

#include "types.h"
#include "error.h"
#include "eval.h"

int eval_binop_nbr_arith(int lhs, e_op op, int rhs)
{
    switch (op)
    {
        case ADD:
            return (lhs + rhs);
        case SUB:
            return (lhs - rhs);
        case MUL:
            return (lhs * rhs);
        case DIV:
            return (lhs / rhs);
        case MOD:
            return (lhs % rhs);
        default:
            break;
    }

    return (666);
}

int eval_binop_nbr_comp(int lhs, e_op op, int rhs)
{
    switch (op)
    {
        case LESS:
            return (lhs < rhs);
        case MORE:
            return (lhs > rhs);
        case EQ:
            return (lhs == rhs);
        case LEQ:
            return (lhs <= rhs);
        case MOQ:
            return (lhs >= rhs);
        default:
            return (666);
    }
}
s_value eval_binop_nbr(s_value *lhs, e_op op, s_value *rhs)
{
    s_value val;


    if (is_binop(op))
    {
        val.type = NUMBER;
        val.as_int = eval_binop_nbr_arith(lhs->as_int, op,rhs->as_int);
    }
    else if (is_comp_op(op))
    {
        val.type = BOOL;
        val.as_int = eval_binop_nbr_comp(lhs->as_int, op, rhs->as_int);
    }

    return (val);
}

s_value eval_binop_char(s_value *lhs, e_op op, s_value *rhs)
{
    s_value val;
    val.type = CHAR;

    switch (op)
    {
        case ADD:
            val.as_char = lhs->as_char + rhs->as_char;
            break;
        case SUB:
            val.as_char = lhs->as_char - rhs->as_char;
            break;
        case MUL:
            val.as_char = lhs->as_char * rhs->as_char;
            break;
        case DIV:
            val.as_char = lhs->as_char / rhs->as_char;
            break;
        case MOD:
            val.as_char = lhs->as_char % rhs->as_char;
            break;
        default:
            break;
    }

    return (val);
}

s_value eval_binop(s_ast *ast)
{
    s_value res;
    s_value lhs = eval(ast->as_bop.lhs);
    s_value rhs = eval(ast->as_bop.rhs);

    if (lhs.type == rhs.type && lhs.type == NUMBER)
        res = eval_binop_nbr(&lhs, ast->as_bop.op, &rhs);
    else if (lhs.type == rhs.type && lhs.type == CHAR)
        res = eval_binop_char(&lhs, ast->as_bop.op, &rhs);
    else
        printf("Type error\n"); // FIXME
    return (res);
}

s_value eval_unop(s_ast *ast)
{
    s_value val;

    switch (ast->as_unop.op)
    {
        case SUB:
            val = eval(ast->as_unop.rhs);
            val.as_int *= -1;
            break;
        case INCREMENT:
            // FIXME
            break;
        case DECREMENT:
            // FIXME
            break;
        case ADD:
            break;
        default:
            break;
    }

    return (val);
}

s_value eval(s_ast *ast)
{
    switch (ast->type)
    {
        case VALUE:
            return (ast->as_value);
        case BINOP:
            return (eval_binop(ast));
        case UNOP:
            return (eval_unop(ast));
        case FUNCALL:
            // FIXME funcall
            break;
        case VAR:
            // FIXME var
            break;
        default:
            exit(1);
    }
    exit(1);
}

