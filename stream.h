/*
** stream.h for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 sanche_g
** Last update 2012-10-07 18:25 vermeille
*/

#ifndef STREAM_H_
# define STREAM_H_

# include "ast.h"

typedef struct
{
    char *content;
    int  index;
} s_stream;

typedef s_stream *t_stream;

char current_char(t_stream s);
void eat_char(t_stream s);
t_stream stream_init(char *str);
void stream_destroy(t_stream s);

#endif /* !STREAM_H_ */

