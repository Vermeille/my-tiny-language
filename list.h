/*
** list.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-08 vermeille
** Last update 2012-10-08 14:53 vermeille
*/

#ifndef LIST_H_
# define LIST_H_

#define LIST(Type)                                     \
    typedef struct list_##Type *s_list_##Type;         \
                                                       \
    struct list_##Type                                 \
    {                                                  \
        Type **values;                                  \
        int size;                                      \
        int max_size;                                  \
    };                                                 \
                                                       \
    s_list_##Type list_##Type##_init(void)             \
    {                                                  \
        s_list_##Type l;                               \
                                                       \
        l = malloc(sizeof (struct list_##Type));       \
        l->values = malloc(sizeof (Type*) * 8);         \
        l->size = 0;                                   \
        l->max_size = 8;                               \
                                                       \
        return (l);                                    \
    }                                                  \
                                                       \
    void list_##Type##_add(s_list_##Type l, Type *el)   \
    {                                                  \
        if (size == max_size)                          \
        {                                              \
            max_size *= 2;                             \
            l->values = realloc(l->values, max_size);  \
        }                                              \
                                                       \
        l->values[size] = el;                          \
        ++l->size;                                     \
    }                                                  \
                                                       \
    void list_##Type##_del(s_list_##Type l, int n)     \
    {                                                  \
        l->values[n] = l->values[size - 1];            \
        --l->size;                                     \
        if (size < max_size / 2)                       \
        {                                              \
            max_size /= 2;                             \
            l->values = realloc(l->values, max_size);  \
        }                                              \
    }                                                  \
                                                       \
    void list_##Type##_destroy(s_list_##Type l)        \
    {                                                  \
        free(l->values);                               \
        free(l);                                       \
    }                                                  \


#endif /* !LIST_H_ */

