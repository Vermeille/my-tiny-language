/*
** eval.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 vermeille
** Last update 2012-10-07 21:17 vermeille
*/

#ifndef EVAL_H_
# define EVAL_H_

# include "ast.h"
# include "eval_types.h"

s_value eval_nbr(s_ast *ast);
s_value eval(s_ast *ast);

#endif /* !EVAL_H_ */

