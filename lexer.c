/*
** lexer.c for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 19:32 sanche_g
** Last update 2012-10-08 05:00 vermeille
*/

#include <stdlib.h>
#include <string.h>

#include "lexer.h"

#include "stream.h"
#include "types.h"
#include "ast.h"

void eat_whitespaces(t_stream s)
{
    while (is_whitespace(current_char(s)))
        eat_char(s);
}

s_token current_token(t_tokenizer t)
{
    return (t->token);
}

void lex_number(t_tokenizer s)
{
    int total = 0;
    s->token.type = T_NUMBER;

    while (is_number(current_char(s->stream)))
    {
        total = total * 10 + current_char(s->stream) - '0';
        eat_char(s->stream);
    }

    s->token.as_nbr = total;
}

void lex_id(t_tokenizer t)
{
    char *id = malloc(256);
    int i = 0;

    t->token.type = T_ID;

    while (is_alpha(current_char(t->stream)))
    {
        id[i++] = current_char(t->stream);
        eat_char(t->stream);
    }

    id[i] = '\0';

    t->token.as_id= id;
}

void lex_test_as_type(t_tokenizer t)
{
    if (current_token(t).type != T_ID)
        return;

    if (!strcmp(current_token(t).as_id, "int"))
    {
        free(current_token(t).as_id);
        t->token.type = T_TYPE;
        t->token.as_type = NUMBER;
    }
    else if (!strcmp(current_token(t).as_id, "char"))
    {
        free(current_token(t).as_id);
        t->token.type = T_TYPE;
        t->token.as_type = CHAR;
    }
    else if (!strcmp(current_token(t).as_id, "string"))
    {
        free(current_token(t).as_id);
        t->token.type = T_TYPE;
        t->token.as_type = STRING;
    }
    else if (!strcmp(current_token(t).as_id, "bool"))
    {
        free(current_token(t).as_id);
        t->token.type = T_TYPE;
        t->token.as_type = BOOL;
    }
}

void lex_char(t_tokenizer t)
{
    if (current_char(t->stream) == '\'')
        eat_char(t->stream);
    t->token.type = T_CHAR;
    t->token.as_char = current_char(t->stream);
    eat_char(t->stream);
    if (current_char(t->stream) == '\'')
        eat_char(t->stream);
    // FIXME

    eat_whitespaces(t->stream);
}

void lex_op(t_tokenizer t)
{
    t->token.type = T_OP;

    if (current_char(t->stream) == '-')
    {
        eat_char(t->stream);
        t->token.as_op = SUB;
    }
    else if (current_char(t->stream) == '/')
    {
        eat_char(t->stream);
        t->token.as_op = DIV;
    }
    else if (current_char(t->stream) == '*')
    {
        eat_char(t->stream);
        t->token.as_op = MUL;
    }
    else if (current_char(t->stream) == '%')
    {
        eat_char(t->stream);
        t->token.as_op = MOD;
    }
    else if (current_char(t->stream) == '(')
    {
        eat_char(t->stream);
        t->token.as_op = OPAR;
    }
    else if (current_char(t->stream) == ')')
    {
        eat_char(t->stream);
        t->token.as_op = CPAR;
    }
    else if (current_char(t->stream) == '+')
    {
        eat_char(t->stream);
        t->token.as_op = ADD;
    }
    else if (current_char(t->stream) == '<')
    {
        eat_char(t->stream);
        if (current_char(t->stream) == '=')
        {
            eat_char(t->stream);
            t->token.as_op = LEQ;
        }
        else
            t->token.as_op = LESS;
    }
    else if (current_char(t->stream) == '>')
    {
        eat_char(t->stream);
        if (current_char(t->stream) == '=')
        {
            eat_char(t->stream);
            t->token.as_op = MOQ;
        }
        else
            t->token.as_op = MORE;
    }
    else if (current_char(t->stream) == '=')
    {
        eat_char(t->stream);
        if (current_char(t->stream) == '=')
        {
            eat_char(t->stream);
            t->token.as_op = EQ;
        }
        else if (current_char(t->stream) == '>')
        {
            eat_char(t->stream);
            t->token.as_op = TYPEOP;
        }
        else
            t->token.as_op = AFFECT;
    }
}

void lex(t_tokenizer t)
{
    eat_whitespaces(t->stream);

    if (is_number(current_char(t->stream)))
        lex_number(t);
    else if (is_alpha(current_char(t->stream)))
        lex_id(t);
    else if (current_char(t->stream) == '\'')
        lex_char(t);
    else if (is_sym(current_char(t->stream)))
        lex_op(t);
}

