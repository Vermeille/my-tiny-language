/*
** parser.c for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 21:47 vermeille
** Last update 2012-10-08 03:17 vermeille
*/

# include <stdlib.h>

#include "parser.h"
#include "types.h"

s_ast *parse_parenthesis(t_tokenizer t)
{
    int sign = 1;
    s_ast *res = NULL;

    if (current_token(t).type == T_OP && current_token(t).as_op == SUB)
    {
        sign = -1;
        lex(t);
    }


    if (t->token.type == T_OP && t->token.as_op == OPAR)
    {
        lex(t);
        res = parse_low(t);

        //if (t->token.type == T_OP && t->token.as_op == CPAR)
        // FIXME Error )
    }
    else
        res = parse_atom(t);

    if (sign == -1)
    {
        s_ast *sign = malloc(sizeof (s_ast));
        sign->type = UNOP;
        sign->as_unop.op = 1;
        sign->as_unop.rhs = res;
        res = sign;
    }

    lex(t);

    return (res);
}

s_ast *parse_high(t_tokenizer t)
{
    s_ast *lhs;

    lhs = parse_parenthesis(t);

    while (current_token(t).type == T_OP && is_high_op(current_token(t).as_op))
    {

        s_ast *res = malloc(sizeof (s_ast));

        res->type = BINOP;
        res->as_bop.op = current_token(t).as_op;

        lex(t);

        res->as_bop.lhs = lhs;
        res->as_bop.rhs = parse_parenthesis(t);
        lhs = res;
    }

    return (lhs);
}

s_ast *parse_low(t_tokenizer t)
{
    s_ast *lhs;

    lhs = parse_high(t);

    while (current_token(t).type == T_OP && is_low_op(current_token(t).as_op))
    {
        s_ast *res = malloc(sizeof (s_ast));

        res->as_bop.op = current_token(t).as_op;
        res->type = BINOP;

        lex(t);

        res->as_bop.lhs = lhs;
        res->as_bop.rhs = parse_high(t);
        lhs = res;
    }

    return (lhs);
}

s_ast *parse_comp(t_tokenizer t)
{
    s_ast *lhs;

    lhs = parse_low(t);

    while (current_token(t).type == T_OP && is_comp_op(current_token(t).as_op))
    {
        s_ast *res = malloc(sizeof (s_ast));

        res->as_bop.op = current_token(t).as_op;
        res->type = BINOP;

        lex(t);

        res->as_bop.lhs = lhs;
        res->as_bop.rhs = parse_low(t);
        lhs = res;

        lex(t);
    }

    return (lhs);
}

s_ast *parse_atom(t_tokenizer t)
{
    s_ast *res = malloc(sizeof (s_ast));

    switch (current_token(t).type)
    {
        case T_NUMBER:
            res->type = VALUE;
            res->as_value.type = NUMBER;
            res->as_value.as_int = current_token(t).as_nbr;
            break;
        case T_CHAR:
            res->type = VALUE;
            res->as_value.type = CHAR;
            res->as_value.as_int = current_token(t).as_char;
            break;
        case T_ID:
            res->type = VAR;
            res->as_id.id = current_token(t).as_id;
            break;
        default:
            break;
    }

    return (res);
}

s_ast *parse_expr(t_tokenizer t)
{
    return (parse_comp(t));
}
