#! /bin/bash
#
# test.sh
# Copyright (C) 2012 vermeille <guillaume.sanchez@epita.fr>
#
# Distributed under terms of the MIT license.
#


./lang << EOF
1 + 2 * 3 + 4
EOF

./lang << PROUT
'e' - 'a' + '0'
PROUT

