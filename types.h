/*
** types.h for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 sanche_g
** Last update 2012-10-07 20:31 vermeille
*/

#ifndef TYPES_H_
# define TYPES_H_

# include "lexer.h"

int is_whitespace(char c);
int is_number(char c);
int is_alpha(char c);
int is_unop(e_op c);
int is_binop(e_op c);
int is_high_op(e_op c);
int is_low_op(e_op c);
int is_comp_op(e_op c);
int is_sym(char c);

#endif /* !TYPES_H_ */

