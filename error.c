/*
** error.c for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 12:45 vermeille
** Last update 2012-10-07 12:48 vermeille
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "error.h"

void error(int err, char *fmt, va_list ap)
{
    fprintf(stderr, fmt, ap);
    exit(err);
}

