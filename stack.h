/*
** stack.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-08 vermeille
** Last update 2012-10-08 14:52 vermeille
*/

#ifndef STACK_H_
# define STACK_H_

# include "list.h"

#define STACK(Type) \
    LIST(Type); \
    typedef s_list_##Type s_stack_##Type; \
    s_stack_##Type stack_##Type##_init(void) \
    { \
        return (list_##Type##_init()); \
    } \
\
    void stack_##Type##_destroy(s_stack_##Type s) \
    { \
        list_##Type##_destroy(s); \
    } \
\
    Type

#endif /* !STACK_H_ */

