/*
** eval_types.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 vermeille
** Last update 2012-10-07 02:48 vermeille
*/

#ifndef EVAL_TYPES_H_
# define EVAL_TYPES_H_

# include "ast.h"

e_value_type eval_type_binop(s_ast *ast);
e_value_type eval_type(s_ast *ast);

#endif /* !EVAL_TYPES_H_ */

