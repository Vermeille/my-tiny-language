/*
** ast.h for lang in lang
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-06 sanche_g
** Last update 2012-10-08 05:00 vermeille
*/

#ifndef AST_H_
# define AST_H_

typedef struct ast s_ast;

typedef enum
{
    VALUE,
    BINOP,
    UNOP,
    VAR,
    FUNCALL
} e_ast_type;

typedef enum
{
    ADD,
    SUB,
    MUL,
    DIV,
    MOD,
    LESS,
    MORE,
    LEQ,
    MOQ,
    EQ,
    NEQ,
    AFFECT,
    TYPEOP,
    INCREMENT,
    DECREMENT,
    OPAR,
    CPAR
} e_op;

typedef struct
{
    int value;
} s_ast_number;

typedef struct
{
    e_op  op;
    s_ast *rhs;
} s_ast_unop;

typedef struct
{
    s_ast *lhs;
    e_op   op;
    s_ast *rhs;
} s_ast_binop;

typedef struct
{
    char *id;
} s_ast_id;

typedef struct
{
    char *name;
} s_ast_funcall;

typedef enum
{
    NUMBER,
    CHAR,
    STRING,
    BOOL
} e_type;

typedef struct
{
    e_type type;
    union
    {
        int   as_int;
        char  as_char;
        char  *as_str;
    };
} s_value;

struct ast
{
    e_ast_type type;
    union
    {
        s_value       as_value;
        s_ast_unop    as_unop;
        s_ast_binop   as_bop;
        s_ast_id      as_id;
    };
};


#endif /* !AST_H_ */

