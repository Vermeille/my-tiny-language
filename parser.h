/*
** parser.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-07 vermeille
** Last update 2012-10-08 03:17 vermeille
*/

#ifndef PARSER_H_
# define PARSER_H_

# include "ast.h"
# include "lexer.h"
# include "stream.h"

s_ast *parse_parenthesis(t_tokenizer s);
s_ast *parse_high(t_tokenizer s);
s_ast *parse_low(t_tokenizer s);
s_ast *parse_id(t_tokenizer s);
s_ast *parse_comp(t_tokenizer t);
s_ast *parse_atom(t_tokenizer t);
s_ast *parse_expr(t_tokenizer t);

#endif /* !PARSER_H_ */

